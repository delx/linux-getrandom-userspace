.PHONY: all clean

all: getrandom.so

clean:
	rm -f getrandom.so

getrandom.so: getrandom.c
	$(CC) -Wall -Wextra -shared $< -o $@
