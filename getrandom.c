#include <fcntl.h>
#include <unistd.h>
#include <sys/random.h>

ssize_t getrandom(void *buf, size_t buflen, unsigned int flags) {
    char* filename = flags & GRND_RANDOM ? "/dev/random" : "/dev/urandom";
    int nonblock = flags & GRND_NONBLOCK ? O_NONBLOCK : 0;
    int oflags = O_CLOEXEC | nonblock;

    int fd = open(filename, oflags);
    if (fd < 0) {
        return -1;
    }

    ssize_t bytes = read(fd, buf, buflen);

    close(fd);

    return bytes;
}
