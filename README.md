# linux-getrandom-userspace

Userspace implementation of the Linux 3.17 getrandom() syscall.

## Overview

Since Linux 3.17 there is a system call `getrandom()`. This was added to glibc 2.25.

This library can be used with `LD_PRELOAD` if you're using Linux 3.16 or earlier and need to run an application which requires `getrandom()`.

See [LWN's The long road to getrandom() in glibc](https://lwn.net/Articles/711013/) for more information.

## Usage

```
make
LD_PRELOAD="${PWD}/getrandom.so" mail
```
